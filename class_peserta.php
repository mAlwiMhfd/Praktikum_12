<?php
/*
MariaDB [dbkegiatan]> desc peserta;
+-------------+-------------+------+-----+---------+----------------+
| Field       | Type        | Null | Key | Default | Extra          |
+-------------+-------------+------+-----+---------+----------------+
| id          | int(11)     | NO   | PRI | NULL    | auto_increment |
| nomor       | varchar(15) | YES  | UNI | NULL    |                |
| email       | varchar(45) | YES  |     | NULL    |                |
| namalengkap | varchar(45) | YES  |     | NULL    |                |
| hp          | varchar(30) | YES  |     | NULL    |                |
| fbaccount   | varchar(30) | YES  |     | NULL    |                |
| kegiatan_id | int(11)     | NO   | MUL | NULL    |                |
| status      | varchar(10) | YES  |     | NULL    |                |
| jenis_id    | int(11)     | NO   | MUL | NULL    |                |
| tgl_daftar  | date        | YES  |     | NULL    |                |
+-------------+-------------+------+-----+---------+----------------+
*/
require_once "DAO.php";
class Peserta extends DAO
{
    public function __construct()
    {
        parent::__construct("peserta");
    }

    public function simpan($data){
        $sql = "INSERT INTO " . $this->tableName ."SET id=?, nomor=?, email=?, namalengkap=?, fbaccount=?, kegiatan_id, status=?, jenis_id=?, tgl_daftar=?";
        $ps = $this->koneksi->prepare(sql);
        $ps->execute($data);
        return $ps->rowCount();
        //die('not implemented yet !!';
     }

    public function ubah($data){
        //die('not implemented yet !!';
    }
}


?>